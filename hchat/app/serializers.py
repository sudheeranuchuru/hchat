from app.models import Message, Channel, MessageChannel
from rest_framework import serializers

class MessageChannelSerializer(serializers.ModelSerializer):
    
    channelname = serializers.SerializerMethodField('get_channel_name')
    username = serializers.SerializerMethodField('get_user_name')
 
    def get_channel_name(self, obj):
        return obj.channel.name

    def get_user_name(self, obj):
        return obj.user.username

    class Meta:
        model = MessageChannel
        field = ('id','channel','channelname','username','code','user',)

