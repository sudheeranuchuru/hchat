from django.shortcuts import render,render_to_response
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import Context, RequestContext
from functools import wraps
from django.db.models import Q
from django.conf import settings
from django.utils.decorators import method_decorator
from django.db.utils import IntegrityError
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from app.models import *
from app.serializers import *
from rest_framework.decorators import api_view

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

def token_required(func):
    def inner(request, *args, **kwargs):
        if request.method == 'OPTIONS':
            return func(request, *args, **kwargs)
        auth_header = request.META.get('HTTP_AUTHORIZATION', None)
        if auth_header is not None:
            tokens = auth_header.split(' ')
            if len(tokens) == 2 and tokens[0] == 'Token':
                token = tokens[1]
                try:
                    request.token = Token.objects.get(key=token)
                    return func(request, *args, **kwargs)
                except Token.DoesNotExist:
                    return JSONResponse({
                        'error': 'Token not found'
                    }, status=401)
        return JSONResponse({
            'error': 'Invalid Header'
        }, status=401)

    return inner


def home_page(request):
    """
    Home Page view function. Will load the index.html  
    """
    return render(request, 'index.html',{})

@csrf_exempt
def register(request):
    if request.method == 'POST':
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        email = request.POST.get('email', None)

        if username is not None and password is not None and email is not None:
            try:
                user = User.objects.create_user(username, email, password)
            except IntegrityError:
                return JSONResponse({
                    'error': 'User already exists'
                }, status=400)
            token, created = Token.objects.get_or_create(user=user)
            print 'success'
            return JSONResponse({
                'token': token.key,
                'username': user.username
            })
        else:
            return JSONResponse({
                'error': 'Invalid Data'
            }, status=400)
    else:
        return JSONResponse({
            'error': 'Invalid Method'
        }, status=405)


@csrf_exempt
def login(request):
    if request.method == 'POST':
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)

        if username is not None and password is not None:
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    token, created = Token.objects.get_or_create(user=user)
                    return JSONResponse({
                        'token': token.key,
                        'username': user.username
                    })
                else:
                    return JSONResponse({
                        'error': 'Invalid User'
                    }, status=400)
            else:
                return JSONResponse({
                    'error': 'Invalid Username/Password'
                }, status=400)
        else:
            return JSONResponse({
                'error': 'Invalid Data'
            }, status=400)
    else:
        return JSONResponse({
            'error': 'Invalid Method'
        }, status=405)


@csrf_exempt
@token_required
def logout(request):
    if request.method == 'POST':
        request.token.delete()
        return JSONResponse({
            'status': 'success'
        })
    else:
        return JSONResponse({
            'error': 'Invalid Method'
        }, status=405)



@csrf_exempt
@api_view(['GET'])
def get_chatrooms(request):
    if request.method == 'GET':
        channels = MessageChannel.objects.filter(user=request.user).filter(subscribe=True)
        serializer = MessageChannelSerializer(channels, many=True)
        return JSONResponse(serializer.data)
    else:
        return JSONResponse({'error': 'Invalid Method',}, status=405)
