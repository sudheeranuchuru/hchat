from django.contrib import admin
from app.models import Message, Channel, MessageChannel


admin.site.register(Channel)
admin.site.register(MessageChannel)
admin.site.register(Message)

