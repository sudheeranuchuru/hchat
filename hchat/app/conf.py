from django.conf import settings


CHAT_ROUTER = getattr(settings, 'ROUTER', 'app.message_router.MessageRouter')
CHAT_ENGINE = getattr(settings, 'ENGINE', 'app.engine')

