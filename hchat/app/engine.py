import json

from django.dispatch import receiver
from django.utils.translation import ugettext as _
from django.utils.module_loading import import_string
from channels.sessions import channel_session
from channels import Group
from django.contrib.auth.models import User
from app.models import *
from app import messages
from app import conf


@channel_session
def on_connect(message):
    payload = message.content['text']
    message.channel_session['user'] = payload['username']
    room = MessageChannel.objects.get(code=payload['room'])
    messagechannel = room.channel
    for msgchannel in messagechannel.messagechannels.all():
        if msgchannel != room:
            msgchannel.websocket_group.add(message.reply_channel)
            msgchannel.websocket_group.send(messages.info(_('User %(username)s joined chat') % payload))
        else:
            for msg in Message.objects.filter(channel=room.channel).filter(user=room.user).filter(show=True):
                msgchannel.websocket_group.add(message.reply_channel)
                msgchannel.websocket_group.send(messages.info(text=msg.message,user=msg.sent_by.username,room=room.code)) 
            #Show old messages


@channel_session
def on_disconnect(message):
    try:
        payload = message.content['text']
        message.channel_session['user'] = payload['username']
        room = MessageChannel.objects.get(code=payload['room'])
        messagechannel = room.channel
        for msgchannel in messagechannel.messagechannels.all():
            if msgchannel != room:
                msgchannel.websocket_group.discard(message.reply_channel)
                msgchannel.websocket_group.send(messages.info(_('User %(username)s left chat') % payload))

    except:
        msgchannel.websocket_group.discard(message.reply_channel)

@channel_session
def on_message(message):
    payload = message.content['text']
    user = payload['username']
    room = MessageChannel.objects.get(code=payload['room'])
    messagechannel = room.channel
    for msgchannel in messagechannel.messagechannels.all():
        #Save Messages
        msg = Message()
        msg.channel = msgchannel.channel
        msg.user = msgchannel.user
        msg.sent_by = room.user
        msg.message = payload['text']
        msg.save()
        #msgchannel.websocket_group.add(message.reply_channel)
        msgchannel.websocket_group.send(messages.info(text=payload['text'], user=user,room=room.code)) 

@channel_session
def on_delete(message):
    payload = message.content['text']
    user = payload['username']
    room = MessageChannel.objects.get(code=payload['room'])
    for msg in Message.objects.filter(channel=room.channel).filter(user=room.user):
        msg.show = False
        msg.save()

def get_engine():
    return import_string(conf.CHAT_ENGINE)
