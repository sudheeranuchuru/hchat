from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import User
import os, random, collections, json
from string import ascii_lowercase
from datetime import datetime, timedelta, date
from channels import Group
#To Create User Token Automatically
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@python_2_unicode_compatible  # only if you need to support Python 2
class Channel(models.Model):
    name = models.CharField(unique=True, max_length=255, default='Public', verbose_name='Channel Name')
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User,related_name="channels", verbose_name='User Name')
    deactivate = models.BooleanField(default=False, verbose_name='De-activate the Channel')

    def __str__(self):              # __unicode__ on Python 2
        return self.name


@python_2_unicode_compatible  # only if you need to support Python 2
class MessageChannel(models.Model):
    channel = models.ForeignKey(Channel,related_name="messagechannels", verbose_name='Channel')
    user = models.ForeignKey(User,related_name="messagechannels",verbose_name='User Name')
    subscribe = models.BooleanField(default=True,verbose_name='Subscribe to Channel')
    code = models.SlugField(unique=True, editable=False) #To identify the channel, used in message channels

    def __str__(self):              # __unicode__ on Python 2
        return  '%s - %s' % (self.channel, self.code)
    
    def save(self, force_insert=False, force_update=False):
        if not self.code:
            self.code = ''.join(random.choice(ascii_lowercase) for i in range(32))
        super(MessageChannel, self).save(force_insert, force_update)


    @property
    def websocket_group(self):
        """
        Returns the Channels Group that sockets should subscribe to to get sent
        messages as they are generated.
        """
        return Group("%s" % self.code)

    def send_message(self, message, user, msg_type='message'):
        """
        Called to send a message to the room on behalf of a user.
        """
        final_msg = {'room': str(self.code), 'message': message, 'username': user.username, 'msg_type': msg_type}

        # Send out the message to everyone in the room
        self.websocket_group.send(
            {"text": json.dumps(final_msg)}
        )



@python_2_unicode_compatible  # only if you need to support Python 2
class Message(models.Model):
    channel = models.ForeignKey(Channel,related_name="messages", verbose_name='Channel')
    user = models.ForeignKey(User,related_name="messages",verbose_name='User Name')
    sent_by = models.ForeignKey(User,related_name="messages_sent",verbose_name='Sent User', null=True)
    message = models.TextField(verbose_name='Message')
    created_on = models.DateTimeField(auto_now_add=True)
    show = models.BooleanField(default=True,verbose_name='Show Message')

    def __str__(self):              # __unicode__ on Python 2
        return  '%s - %s - %s' % (self.channel, self.user, self.message)



@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
        if not Channel.objects.all():
            channel = Channel()
            channel.name = 'Public'
            channel.created_by = instance
            channel.save()
            for c in Channel.objects.all():
                if not instance.messagechannels.filter(channel=c).exists():
                    msgchannel = MessageChannel()
                    msgchannel.channel = c
                    msgchannel.user = instance
                    msgchannel.save()
        else:
            for c in Channel.objects.all():
                if not instance.messagechannels.filter(channel=c).exists():
                    msgchannel = MessageChannel()
                    msgchannel.channel = c
                    msgchannel.user = instance
                    msgchannel.save()
