//App Name "ChatApp"
var ChatApp = angular.module('ChatApp', [
                'ngCookies',
                'ngResource',
                'ngSanitize',
                'ngRoute'
                ]);

ChatApp.controller('AuthCtrl', function ($scope, $location,$window, AuthService) {
  if ($window.localStorage.token) {
    $location.path('/chats');
    return;
  }
  $scope.login = function () {
    var username = $scope.username;
    var password = $scope.password;

    if (username && password) {
      AuthService.login(username, password).then(
        function () {
          $location.path('/chats');
        },
        function (error) {
          $scope.LoginError = error;
        }
      );
    } else {
      $scope.LoginError = 'Username and password required';
    }
  };
});


ChatApp.controller('RegisterCtrl', function ($scope, $location, AuthService) {
  $scope.register = function () {
    var username = $scope.username;
    var email = $scope.email;
    var password = $scope.password;

    if (username && password && email) {
      AuthService.register(username, password, email).then(
        function () {
          $location.path('/chats');
        },
        function (error) {
          $scope.RegisterError = error;
        }
      );
    } else {
      $scope.RegisterError = 'All Fields Required';
    }
  };
});



ChatApp.controller('HomePageCtrl', function ($scope, $window, $location, $http, AuthService, API_SERVER) {
  if (!$window.localStorage.token) {
    $location.path('/chats');
    return;
  }

  var url = API_SERVER + 'chatrooms';
  $scope.token = $window.localStorage.token;
  $scope.username = $window.localStorage.username;

  $http.get(url).success(function(data, status, headers, config) {
        $scope.chatrooms = data;
        console.log(data);
    });

  $scope.logout = function () {
    AuthService.logout().then(
      function () {
        $location.path('/');
      },
      function (error) {
        $scope.error = error;
      }
    );
  };

});

ChatApp.factory('AuthService', function ($http, $window, $q, API_SERVER) {

  var authenticate = function (username, password, endpoint) {
    var url = API_SERVER + endpoint;
    var deferred = $q.defer();

    $http.post(url, 'username=' + username + '&password=' + password, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(
      function (response) {
        var token = response.data.token;
        var username = response.data.username;

        if (token && username) {
          $window.localStorage.token = token;
          $window.localStorage.username = username;
          deferred.resolve(true);
        } else {
          deferred.reject('Invalid data received from server');
        }
      },
      function (response) {
        deferred.reject(response.data.error);
      }
    );
    return deferred.promise;
  };

  var registration = function (username, password, email, endpoint) {
    var url = API_SERVER + endpoint;
    var deferred = $q.defer();

    $http.post(url, 'username=' + username + '&password=' + password + '&email=' + email, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(
      function (response) {
        var token = response.data.token;
        var username = response.data.username;

        if (token && username) {
          $window.localStorage.token = token;
          $window.localStorage.username = username;
          deferred.resolve(true);
        } else {
          deferred.reject('Invalid data received from server');
        }
      },
      function (response) {
        deferred.reject(response.data.error);
      }
    );
    return deferred.promise;
  };


  var logout = function () {
    var deferred = $q.defer();
    var url = API_SERVER + 'logout/';

    $http.post(url).then(
      function () {
        $window.localStorage.removeItem('token');
        $window.localStorage.removeItem('username');
        deferred.resolve();
      },
      function (error) {
        deferred.reject(error.data.error);
      }
    );
    return deferred.promise;
  };

  return {
    register: function (username, password, email) {
      return registration(username, password, email, 'register/');
    },
    login: function (username, password) {
      return authenticate(username, password, 'login/');
    },
    logout: function () {
      return logout();
    }
  };

});


ChatApp.factory('AuthInterceptor', function ($rootScope, $q, $window, $location) {
  return {
    request: function (config) {
      config.headers = config.headers || {};
      if ($window.localStorage.token) {
        config.headers.Authorization = 'Token ' + $window.localStorage.token;
      }
      return config;
    },

    responseError: function (response) {
      if (response.status === 401) {
        $window.localStorage.removeItem('token');
        $window.localStorage.removeItem('username');
        $location.path('/');
        return;
      }
      return $q.reject(response);
    }
  };
});

ChatApp.config(function ($routeProvider, $httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.defaults.withCredentials = true;
        $routeProvider
            .when('/', {
                templateUrl: 'static/assets/partials/auth.html',
                controller: 'AuthCtrl'
                })
            .when('/register', {
                templateUrl: 'static/assets/partials/register.html',
                controller: 'RegisterCtrl'
                })
            .when('/chats', {
                templateUrl: 'static/assets/partials/home.html',
                controller: 'HomePageCtrl'
                })
            .when('/chat/:code', {
                templateUrl: 'static/assets/partials/messages.html',
                controller: 'ChatCtrl'
                })
            .otherwise({
                redirectTo: '/chats'
                });
});

ChatApp.constant('API_SERVER', window.location.origin + '/api/v1/');


ChatApp.controller('ChatCtrl', function ($scope, $window, $location, $http, AuthService, API_SERVER,$routeParams) {
  console.log($routeParams.code);
  if (!$window.localStorage.token) {
    $location.path('/chats');
    return;
  };
  $scope.activeuser = $window.localStorage.username;
  $scope.room = $routeParams.code;
  var dataStream = new WebSocket('wss://' + window.location.hostname + '/chat');
  $scope.messages = [];

  dataStream.onmessage = function(message) {
        $scope.messages.push(JSON.parse(message.data));
      };
 
  dataStream.onopen = function() {
        dataStream.send(JSON.stringify({'type':'connect','text':'','username':$scope.activeuser, 'room': $scope.room }));
      };

  $scope.sendmessage =  function() {
        console.log($scope.msgvalue);
        if ($scope.msgvalue) {
            dataStream.send(JSON.stringify({'type':'message', 'text':$scope.msgvalue , 'username':$scope.activeuser, 'room': $scope.room }));
        };
        console.log($scope.activeuser);
        $scope.msgvalue = null;
    };

  $scope.clearchat =  function() {
        dataStream.send(JSON.stringify({'type':'delete','text':'','username':$scope.activeuser, 'room': $scope.room }));
        $scope.messages = [];
    };

  $scope.logout = function () {
    AuthService.logout().then(
      function () {
        $location.path('/');
      },
      function (error) {
        $scope.error = error;
      }
    );
  };

});


