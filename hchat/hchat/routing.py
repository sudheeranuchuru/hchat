from channels.routing import route, include

from app.routing import routing as app_routing


routing = [
    include(app_routing, path=r"^/chat"),
]

